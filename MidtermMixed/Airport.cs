﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MidtermMulti
{
    class Airport
    {
        // Constructors
        public Airport(string code, string city, double lat, double lng, int elevM)
        {
            Code = code;
            City = city;
            Latitude = lat;
            Longitude = lng;
            elevationMeters = elevM;
        }
        public Airport(string line)
        {
            string[] splitLine = line.Split(';');
            if (splitLine.Length != 5)
            {
                Program.Logger?.Invoke("Invalid number of items in line");
                throw new ArgumentException("Invalid number of items in line");
            }
            Code = splitLine[0];
            City = splitLine[1];
            if (!double.TryParse(splitLine[2], out double lat)) //FormatException
            {
                Program.Logger?.Invoke("Latitude in split line data must be a floating point number");
                throw new ArgumentException("Latitude in split line data must be a floating point number");
            }
            Latitude = lat;
            if (!double.TryParse(splitLine[3], out double lng)) //FormatException
            {
                Program.Logger?.Invoke("Longitude in split line data must be a floating point number");
                throw new ArgumentException("Longitude in split line data must be a floating point number");
            }
            Longitude = lng;
            if (!int.TryParse(splitLine[4], out int elevM)) //FormatException
            {
                Program.Logger?.Invoke("Elevation in split line data must be an integer");
                throw new ArgumentException("Elevation in split line data must be an integer");
            }
            elevationMeters = elevM;
        }

        // Fields and Properties
        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if(Regex.IsMatch(value, @"/^[A-Z]{3}$/gm"))
                {
                    Program.Logger?.Invoke("Must contain three capital letters only");
                    throw new ArgumentException("Must contain three capital letters only");
                }
                _code = value;
            }
        }
        private string _city;
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    Program.Logger?.Invoke("City must be between 1 and 50 characters, and not contain a semicolon");
                    throw new ArgumentException("City must be between 1 and 50 characters, and not contain a semicolon");
                }
                _city = value;
            }
        }
        private double _latitude;
        public double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                if (value < -90 || value > 90)
                {
                    Program.Logger?.Invoke("Latitudee must be between -90 and 90");
                    throw new ArgumentException("Latitudee must be between -90 and 90");
                }
                _latitude = value;
            }
        }
        private double _longitude;
        public double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                if (value < -180 || value > 180)
                {
                    Program.Logger?.Invoke("Longitude must be between -180 and 180");
                    throw new ArgumentException("Longitude must be between -180 and 180");
                }
                _longitude = value;
            }
        }
        private int _elevationMeters;
        public int elevationMeters
        {
            get
            {
                return _elevationMeters;
            }
            set
            {
                if (value < -1000 || value > 10000)
                {
                    Program.Logger?.Invoke("Elevation (in meters) must be between -1000 and 10000");
                    throw new ArgumentException("Elevation (in meters) must be between -1000 and 10000");
                }
                _elevationMeters = value;
            }
        }

        // ToString and ToDataString
        public override string ToString()
        {
            return $"{Code} in {City} at {Latitude} lat / {Longitude} lng at {elevationMeters} elevation";
        }

        public string ToDataString()
        {
            return $"{Code};{City};{Latitude};{Longitude};{elevationMeters}";
        }
    }
}
