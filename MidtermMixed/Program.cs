﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermMulti
{
    class Program
    {
        // List
        static List<Airport> AirportList = new List<Airport>();

        // Menu
        public static void Menu()
        {
            while(true)
            {
                Console.Write("Menu:\n"
                + "1. Add Airport\n" +
                "2. List all airports\n" +
                "3. Find nearest airport by code\n" +
                "4. Find airports elevation's standard deviation\n" +
                "5. Print Nth super-Fibonacci\n" +
                "6. Change log delegates\n" +
                "0. Exit\n" +
                "Enter your choice: ");

                    if (!int.TryParse(Console.ReadLine(), out int choice))
                    {
                        Console.WriteLine("Invalid option. Choose again");
                    }
                    switch (choice)
                    {
                        case 1:
                            AddAirport();
                            break;
                        case 2:
                            ListAirports();
                            break;
                        case 3:
                            FindClosestAirport();
                            break;
                        case 4:
                            ElevationStandardDeviation();
                            break;
                        case 5:
                            //TO DO;
                            break;
                        case 6:
                            LoggingSettings();
                            break;
                        case 0:
                            Console.WriteLine("Exiting");
                            return;
                        default:
                            Console.WriteLine("Invalid option.");
                            break;
                    }                
            }
            
        }

        // Add an Airport
        public static void AddAirport()
        {
            Console.WriteLine("Adding airport");
            Console.Write("Enter code: ");
            string code = Console.ReadLine();
            Console.Write("Enter city: ");
            string city = Console.ReadLine();
            Console.Write("Enter latitude: ");
            if (!double.TryParse(Console.ReadLine(), out double lat))
            {
                Console.WriteLine("Please enter a decimal number between -90 and 90");
            }
            Console.Write("Enter longitude: ");
            if (!double.TryParse(Console.ReadLine(), out double lng))
            {
                Console.WriteLine("Please enter a decimal number between -180 and 180");
            }
            Console.Write("Enter elevation in meters: ");
            if (!int.TryParse(Console.ReadLine(), out int elevM))
            {
                Console.WriteLine("Please enter an integer between -1000 and 10000");
            }
            Airport airport = new Airport(code, city, lat, lng, elevM);
            AirportList.Add(airport);
        }

        //List all Airports
        public static void ListAirports()
        {
            foreach(Airport airport in AirportList)
            {
                Console.WriteLine(airport);
            }
        }

        //Find closest airport
        public static void FindClosestAirport()
        {
            double fromLatitude = 0;
            double fromLongitude = 0;
            double toLatitude = 0;
            double toLongitude = 0;

            Console.Write("Enter airport code: ");
            string code = Console.ReadLine();
            foreach(Airport airport in AirportList)
            {
                if(airport.Code.Equals(code))
                {
                    fromLatitude = airport.Latitude;
                    fromLongitude = airport.Longitude;
                }
            }
            GeoCoordinate distFrom = new GeoCoordinate(fromLatitude, fromLongitude);
            GeoCoordinate distTo = new GeoCoordinate(toLatitude, toLongitude);

        }

        //Standard Deviation of Airport Elevation
        public static void ElevationStandardDeviation()
        {
            int elevSum = AirportList.Sum(a => a.elevationMeters);
            double elevAvg = elevSum / AirportList.Count;

            double elevSumOfSquares = AirportList.Sum(a => (a.elevationMeters - elevAvg) * (a.elevationMeters - elevAvg));
            double elevStdDev = Math.Sqrt(elevSumOfSquares / AirportList.Count);
            Console.WriteLine("For all airports the standard deviation is {0:0.##}", elevStdDev);
        }

        //********REMEMBER FIBONACCI***********

        //Delegates for Logging
        static string path2 = @"..\..\events.log";
        public delegate void LoggerDelegate(string msg);
        public static LoggerDelegate Logger;

        public static void LogToConsole(string msg)
        {
            Console.WriteLine("*** LOG: " + msg);
        }
        public static void LogToFile(string msg)
        {
            try
            {
                File.AppendAllText(path2, msg + "\n"); //IOException + SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error saving log message to file: " + ex.Message);
            }
        }

        public static void LoggingSettings()
        {
           Logger = null;
            Console.Write("Changing logging settings:\n"
                + "1-Logging to console\n" +
                "2-Logging to file" +
                "\nEnter your choices, comma-separated, empty for none: ");
            string choice = Console.ReadLine();
            if (choice.Equals("1"))
            {
                Logger = LogToConsole;
                Console.WriteLine("Logging to console");
            }
            else if (choice.Equals("2"))
            {
                Logger = LogToFile;
                Console.WriteLine("Logging to file");
            }
            else if (choice.Equals("1,2"))
            {
                Logger = LogToConsole;
                Logger += LogToFile;
                Console.WriteLine("Logging to console and to file");
            } else
            {
                Console.WriteLine("Logging disabled");
            }
                
            

            /*switch (choice)
            {
                case 1:
                    Logger = LogToConsole;
                    Console.WriteLine("Logging to console");
                    break;
                case 2:
                    Logger = LogToFile;
                    Console.WriteLine("Logging to file");
                    break;
                case :
                    // HOW TO DO TWO CHOICES
                    break;
                default:
                    Console.WriteLine("Logging disabled");
                    break;
            } */
        }

        //Reading from file
        static string path = @"..\..\data.txt";
        static void ReadDataFromFile()
        {
            try
            {
                if (!File.Exists(path))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(path); // IOException and SystemException
                foreach (String line in readText)
                {
                    try
                    {
                        Airport airport = new Airport(line);
                        AirportList.Add(airport);
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        Console.WriteLine($"Error: {ex.Message} in:\n {line}");
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }

        //Write to file
        static void WriteDataToFile()
        {
            try
            {
                var linesList = AirportList.Select(a => a.ToDataString());
                File.WriteAllLines(path, linesList); //IOException
                Console.WriteLine("Data saved to file");
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing data to file: " + ex.Message);
            }
        }
        static void Main(string[] args)
        {
            ReadDataFromFile();
            Menu();
            WriteDataToFile();
        }
    }
}
