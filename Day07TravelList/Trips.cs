﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Day07TravelList
{
    class Trips
    {
        //Data Exception
        public class InvalidDataException : Exception
        {
            public InvalidDataException(string msg) : base(msg) { }
            public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
        }

        //Constructors
        public Trips(string destination, string travelerName, string travelerPassport, DateTime departureDate, DateTime returnDate)
        {
            Destination = destination;
            TravelerName = travelerName;
            TravelerPassport = travelerPassport;
            SetDepartureReturnDates(departureDate, returnDate);
        }
        public Trips(string dataLine)
        {
            try
            {
                string[] splitLine = dataLine.Split(';');
                if (splitLine.Length != 5)
                {
                    throw new ArgumentException("Invalid number of items in line");
                }
                Destination = splitLine[0];
                TravelerName = splitLine[1];
                TravelerPassport = splitLine[2];
                DateTime depDate = DateTime.ParseExact(splitLine[3], "yyyy-MM-dd", CultureInfo.InvariantCulture); // ex
                DateTime retDate = DateTime.ParseExact(splitLine[4], "yyyy-MM-dd", CultureInfo.InvariantCulture); // ex
                SetDepartureReturnDates(depDate, retDate);
            } catch(FormatException ex)
            {
                throw new InvalidDataException("Data format invalid in line", ex);
            }
        }

        //Fields
        private string _destination;
        public string Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9 \.]{2,30}$"))
                {
                    throw new InvalidDataException("Destination must be 2-30 characters long");
                }
                _destination = value;
            }
        }

        private string _travelerName;
        public string TravelerName
        {
            get
            {
                return _travelerName;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9 \.]{2,30}$"))
                {
                    throw new InvalidDataException("Traveler name must be 2-30 characters long");
                }
                _travelerName = value;
            }
        }

        private string _travelerPassport;
        public string TravelerPassport
        {
            get
            {
                return _travelerPassport;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{2}[0-9]{6}$"))
                {
                    throw new InvalidDataException("Passport must be in AA123456 format");
                }
                _travelerPassport = value;
            }
        }

        private DateTime _departureDate, _returnDate;
        public DateTime DepartureDate
        {
            get { return _departureDate; }
        }
        public DateTime ReturnDate
        {
            get { return _returnDate; }
        }
        public void SetDepartureReturnDates(DateTime dep, DateTime ret)
        {
            if (ret < dep)
            {
                throw new InvalidDataException("Return can't be before departure");
            }
            _departureDate = dep;
            _returnDate = ret;
        }

        //ToString and ToDataString
        public override string ToString()
        {
            //string depDate = DateTime.ToString("yyyy-MM-dd");
           // string retDate = DateTime.ToString("yyyy-MM-dd");
            return $"{TravelerName}({TravelerPassport}) to {Destination} on {DepartureDate:d} returning {ReturnDate:d}";
        }
        public string ToDataString()
        {
            return $"{Destination};{TravelerName};{TravelerPassport};{DepartureDate:yyyy-MM-dd};{ReturnDate:yyyy-MM-dd}";
        }
    }
}
