﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07TravelList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //***List and path
        List<Trips> TripList = new List<Trips>();
        const string path = @"..\..\trips.txt";

        //***Constructor
        public MainWindow()
        {
            InitializeComponent();
            lvTripList.ItemsSource = TripList;
            ReadTripsFromFile();
        }

        //***Read trips from file
        void ReadTripsFromFile()
        {
            try
            {
                if (!File.Exists(path))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(path); // IOException and SystemException
                foreach (String line in readText)
                {
                    try
                    {
                        Trips trip = new Trips(line);
                        TripList.Add(trip);
                        lvTripList.Items.Refresh();
                        //lvTrips.Items.Add(trip);
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

        }

        //***Write trips to file
        void SaveTripsToFile(string fileName, List<Trips>tripsToSave)
        {
            try
            {
                List<string> createText = new List<string>();
                foreach (Trips trip in tripsToSave)
                {
                    createText.Add(trip.ToDataString());
                }
                File.WriteAllLines(fileName, createText); // IOException and SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

        }

        //***Add trip to ListView
        private void btAddTrip_Click(object sender, RoutedEventArgs e)
        {
            // Get Destination
            if (tbDestination.Text.Contains(";"))
            {
                MessageBox.Show("Destination cannot contain a semicolon", "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            string destination = tbDestination.Text;

            // Get Name
            if (tbName.Text.Contains(";"))
            {
                MessageBox.Show("Name cannot contain a semicolon", "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            string name = tbName.Text;

            // Get Passport Number
            string passport = tbPassportNo.Text;

            // Get Departure Date
            DateTime depDate = (DateTime)dpDeparture.SelectedDate;

            // Get Return Date
            DateTime retDate = (DateTime)dpReturn.SelectedDate;

            // Create and add trip to List and ListView
            Trips trip = new Trips(destination, name, passport, depDate, retDate);
            TripList.Add(trip);
            lvTripList.Items.Refresh(); //inform listview that data has changed
                                     //lvTrips.Items.Add(trip);

            // Clear the inputs
            tbDestination.Clear();
            tbName.Clear();
            tbPassportNo.Clear();
            dpDeparture.SelectedDate = null;
            dpReturn.SelectedDate = null;
        }

        //***Save selected trips and export
        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Process save file dialog box results
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    var selItems = lvTripList.SelectedItems.Cast<Trips>().ToList();
                    if (selItems.Count == 0)
                    {
                        MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    // Save document
                    string fileName = dlg.FileName;
                    SaveTripsToFile(fileName, selItems);
                }
                catch (Exception ex) when (ex is IOException || ex is SystemException)
                {
                    MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

            }
        }

        //***Ask user to save upon closing window
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Yes to save trips and close this window, No to close without saving, and Cancel to cancel closing",
            "Confirmation", MessageBoxButton.YesNoCancel);
            
            if (result == MessageBoxResult.Yes)
            {
                SaveTripsToFile(path, TripList);
            }
            else if(result == MessageBoxResult.No)
            {
                // No code here  
            }
            else
            {
                e.Cancel = true; 
            }
        }
    }
}
