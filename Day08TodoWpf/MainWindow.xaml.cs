﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08TodoWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //***List and path and edited Todo
        List<Todo> TodoList = new List<Todo>();
        const string path = @"..\..\todos.txt";
        Todo currentEditedTodo = null;

        //***Constructor
        public MainWindow()
        {
            InitializeComponent();
            cbStatus.ItemsSource = Enum.GetValues(typeof(Day08TodoWpf.EStatus)).Cast<Day08TodoWpf.EStatus>();
            lvTodos.ItemsSource = TodoList;
            ReadTodosFromFile();
        }

        //***Read todos from file
        void ReadTodosFromFile()
        {
            try
            {
                if (!File.Exists(path))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(path); // IOException and SystemException
                foreach (String line in readText)
                {
                    try
                    {
                        Todo todo = new Todo(line);
                        TodoList.Add(todo);
                        lvTodos.Items.Refresh();
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        //***Write todos to file
        void SaveTodosToFile(string fileName, List<Todo> todosToSave)
        {
            try
            {
                List<string> createText = new List<string>();
                foreach (Todo todo in todosToSave)
                {
                    createText.Add(todo.ToDataString());
                }
                File.WriteAllLines(fileName, createText); // IOException and SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

        }

        //***Add Todos to ListView
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            // Get Task
            if (tbTask.Text.Contains(";"))
            {
                MessageBox.Show("Task cannot contain a semicolon", "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            string task = tbTask.Text;

            // Get Difficulty
            double difficulty = sliderDiff.Value;

            // Get Due Date
            DateTime dueDate = (DateTime)dpDate.SelectedDate;

            // Get Status
            var statusStr = cbStatus.SelectedItem.ToString();
            EStatus status = (EStatus)Enum.Parse(typeof(EStatus), statusStr);

            // Create and add todo to List
            Todo todo = new Todo(task, difficulty, dueDate, status);
            TodoList.Add(todo);
            lvTodos.Items.Refresh(); //inform listview that data has changed

            // Clear the inputs
            tbTask.Clear();
            dpDate.SelectedDate = null;
            cbStatus.SelectedItem = null;
        }

        //***Delete Todos from ListView
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Todo item = (Todo)lvTodos.SelectedItem;
            if (item == null)
            {
                return; //do nothing, no selection
            }
            TodoList.Remove(item);
            lvTodos.Items.Refresh();
        }

        //***Update Todos from ListView
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            Todo item = (Todo)lvTodos.SelectedItem;
            if(item == null)
            {
                return; //do nothing, no selection
            }
            currentEditedTodo = item;
            tbTask.Text = currentEditedTodo.Task;
            sliderDiff.Value = currentEditedTodo.Difficulty;
            dpDate.SelectedDate = currentEditedTodo.DueDate;
            cbStatus.SelectedItem = currentEditedTodo.Status;
            TodoList.Remove(item);
            TodoList.Add(currentEditedTodo);
            lvTodos.Items.Refresh();
        }

        //***Save and Export selected todos to file
        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Process save file dialog box results
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    var selItems = lvTodos.SelectedItems.Cast<Todo>().ToList();
                    if (selItems.Count == 0)
                    {
                        MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    // Save document
                    string fileName = dlg.FileName;
                    SaveTodosToFile(fileName, selItems);
                }
                catch (Exception ex) when (ex is IOException || ex is SystemException)
                {
                    MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

            }
        }

        //**Save upon closing program
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to save before closing?",
            "Confirmation", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Yes)
            {
                SaveTodosToFile(path, TodoList);
            }
            else if (result == MessageBoxResult.No)
            {
                // No code here  
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
