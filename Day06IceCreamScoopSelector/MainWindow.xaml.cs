﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06IceCreamScoopSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<string> FlavourList = new List<string>();
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var flavour = lstFlavourList.SelectedItem;
            try
            {
                string flavourStr = flavour.ToString();
                if (flavour == null)
                {
                    MessageBox.Show("No flavour was selected", "Input Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    FlavourList.Add(flavourStr);
                    lstFlavoursSelected.ItemsSource = FlavourList;
                }
            } catch (Exception ex) when (ex is InvalidCastException || ex is ArgumentException)
            {
                MessageBox.Show("Error", "Input Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var flavour = lstFlavourList.SelectedItem;
            string flavourStr = flavour.ToString();
            FlavourList.Remove(flavourStr);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            FlavourList.Clear();
        }
    }
}
