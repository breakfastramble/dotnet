﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day09TodoWithDialog
{
    //Data Exception
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
    }
    public enum EStatus
    {
        Pending,
        Done,
        Delegated
    } //  matches the ComboBox in GUI
    public class Todo
    {
        //Constructors
        public Todo(string task, double difficulty, DateTime dueDate, EStatus status)
        {
            Task = task;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }
        public Todo(string dataLine)
        {
            try
            {
                string[] splitLine = dataLine.Split(';');
                if (splitLine.Length != 4)
                {
                    throw new ArgumentException("Invalid number of items in line");
                }
                Task = splitLine[0];
                Difficulty = int.Parse(splitLine[1]);
                DueDate = DateTime.ParseExact(splitLine[2], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                Status = (EStatus)Enum.Parse(typeof(EStatus), splitLine[3]);
            }
            catch (FormatException ex)
            {
                throw new InvalidDataException("Data format invalid in line", ex);
            }
        }

        //Fields/Properties
        private string _task;
        public String Task
        {
            get
            {
                return _task;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9 %_(),./\-]{2,30}$"))
                {
                    throw new InvalidDataException("Task must be 1-100 characters long and can only contain uppercase and lowercase letters, digits, spaces, and special characters %_-,./\\()");
                }
                _task = value;
            }
        }
        private double _difficulty;
        public double Difficulty
        {
            get
            {
                return _difficulty;
            }
            set
            {
                _difficulty = value;
            }
        }
        private DateTime _dueDate;
        public DateTime DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                /*System.DateTime year1 = new System.DateTime(1900);
                System.DateTime year2 = new System.DateTime(2100);
                if (value.Year.CompareTo(year1) > 0 || value.Year.CompareTo(year2) < 0)
                {
                    throw new InvalidDataException("Due date must be between year 1900 and 2100");
                }*/
                _dueDate = value;
            }
        }
        private EStatus _status;
        public EStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        //To String and ToDataString
        public override string ToString()
        {
            return $"{Task}; Difficulty: {Difficulty}; Due on: {DueDate:d}; {Status}";
        }
        public string ToDataString()
        {
            return $"{Task};{Difficulty};{DueDate:d};{Status}";
        }
    }
}
