﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09TodoWithDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //***List and path and edited Todo
        List<Todo> TodoList = new List<Todo>();
        const string path = @"..\..\todos.txt";
        //Todo currentEditedTodo = null;

        //***Constructor
        public MainWindow()
        {
            InitializeComponent();
            lvTodos.ItemsSource = TodoList;
            ReadTodosFromFile();
        }

        //***Read todos from file
        void ReadTodosFromFile()
        {
            try
            {
                if (!File.Exists(path))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(path); // IOException and SystemException
                foreach (String line in readText)
                {
                    try
                    {
                        Todo todo = new Todo(line);
                        TodoList.Add(todo);
                        lvTodos.Items.Refresh();
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        //***Write todos to file
        void SaveTodosToFile(string fileName, List<Todo> todosToSave)
        {
            try
            {
                List<string> createText = new List<string>();
                foreach (Todo todo in todosToSave)
                {
                    createText.Add(todo.ToDataString());
                }
                File.WriteAllLines(fileName, createText); // IOException and SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

        }

        //***Add Todo Item
        private void AddTodoMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                string task = dlg.tbTask.Text;
                double difficulty = dlg.sliderDiff.Value;
                DateTime dueDate = (DateTime)dlg.dpDate.SelectedDate;
                var statusStr = dlg.cbStatus.SelectedItem.ToString();
                EStatus status = (EStatus)Enum.Parse(typeof(EStatus), statusStr);
                // Create and add todo to List
                Todo todo = new Todo(task, difficulty, dueDate, status);
                TodoList.Add(todo);
                lvTodos.Items.Refresh(); //inform listview that data has changed
                // Clear the inputs
                dlg.tbTask.Clear();
                dlg.dpDate.SelectedDate = null;
                dlg.cbStatus.SelectedItem = null;
            }
        }

        //***To Update Todos
        private void lvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Todo item = (Todo)lvTodos.SelectedItem; // possible multiple selection
            if (item == null) return;
            AddEditDialog dlg = new AddEditDialog();
            dlg.Owner = this;
            dlg.tbTask.Text = TodoList[lvTodos.SelectedIndex].Task;
            dlg.sliderDiff.Value = TodoList[lvTodos.SelectedIndex].Difficulty;
            dlg.dpDate.SelectedDate = TodoList[lvTodos.SelectedIndex].DueDate;
            dlg.cbStatus.Text = TodoList[lvTodos.SelectedIndex].Status.ToString();
            if (dlg.ShowDialog() == true)
            {
                int index = TodoList.IndexOf(item);
                Todo currentEditedTodo = item;
                dlg.tbTask.Text = currentEditedTodo.Task;
                dlg.sliderDiff.Value = currentEditedTodo.Difficulty;
                dlg.dpDate.SelectedDate = currentEditedTodo.DueDate;
                dlg.cbStatus.SelectedItem = currentEditedTodo.Status;
                TodoList[index] = currentEditedTodo;
                lvTodos.Items.Refresh();
            }
        }

        //***Delete Todo Item
        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {

            MessageBoxResult result = MessageBox.Show("Do you want to delete this todo item?",
            "Confirmation", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                TodoList.Remove(TodoList[lvTodos.SelectedIndex]);
                lvTodos.Items.Refresh();
            }
            else
            {
                return;  
            }
            
        }

        //Exporting multiple lines
        private void ExportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Process save file dialog box results
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    var selItems = lvTodos.SelectedItems.Cast<Todo>().ToList();
                    if (selItems.Count == 0)
                    {
                        MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    // Save document
                    string fileName = dlg.FileName;
                    SaveTodosToFile(fileName, selItems);
                }
                catch (Exception ex) when (ex is IOException || ex is SystemException)
                {
                    MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

            }
        }

        //***Save upon Closing window
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to save before closing?",
            "Confirmation", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Yes)
            {
                SaveTodosToFile(path, TodoList);
            }
            else if (result == MessageBoxResult.No)
            {
                // No code here  
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
