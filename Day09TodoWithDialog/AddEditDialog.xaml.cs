﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09TodoWithDialog
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        public AddEditDialog()
        {
            InitializeComponent();
            cbStatus.ItemsSource = Enum.GetValues(typeof(Day09TodoWithDialog.EStatus)).Cast<Day09TodoWithDialog.EStatus>();
            
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            // Get Task
            if (tbTask.Text.Contains(";"))
            {
                MessageBox.Show("Task cannot contain a semicolon", "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            string task = tbTask.Text;

            // Get Difficulty
            double difficulty = sliderDiff.Value;

            // Get Due Date
            DateTime dueDate = (DateTime)dpDate.SelectedDate;

            // Get Status
            var statusStr = cbStatus.SelectedItem.ToString();
            EStatus status = (EStatus)Enum.Parse(typeof(EStatus), statusStr);
            DialogResult = true;
        }
    }
}
