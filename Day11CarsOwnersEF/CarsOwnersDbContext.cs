using System;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Day11CarsOwnersEF
{
    public class CarsOwnersDbContext : DbContext
    {
        // Your context has been configured to use a 'CarsOwnersDbContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Day11CarsOwnersEF.CarsOwnersDbContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CarsOwnersDbContext' 
        // connection string in the application configuration file.
        const string DbName = "Day11CarsOwnersEFDb.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public CarsOwnersDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Owner> Owners { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}