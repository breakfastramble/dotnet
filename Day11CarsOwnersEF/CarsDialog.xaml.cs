﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
        //Object for editing
        Owner currOwner;

        //***Constructor
        public CarsDialog(Owner owner)
        {
            InitializeComponent();
            currOwner = owner;
            lblOwnerName.Content = currOwner.Name;
            ReloadCarRecordsForCurrOwner();
        }


        //Loading Data from Database
        void ReloadCarRecordsForCurrOwner()
        {
            try
            {
                // we don't necessarily need to run a query here because the data has been loaded already
                List<Car> carList = currOwner.CarsInGarage.ToList<Car>();
                lvCars.ItemsSource = carList;
                Utils.AutoResizeColumns(lvCars);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //***Validate All Inputs method
        bool AreInputsValid()
        {
            if (tbMakeModel.Text.Length < 1 || tbMakeModel.Text.Length > 100)
            {
                MessageBox.Show("Please, fill in Make & Model", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        //***Clear All Inputs method
        public void ClearDialogInputs()
        {
            tbMakeModel.Text = "";
            lblId.Content = "";
        }

        //***Add Car
        private void btAddCar_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            try
            {
                Car c = new Car
                {
                    MakeModel = tbMakeModel.Text,
                    OwnerId = currOwner.Id
                };
                Globals.ctx.Cars.Add(c);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //***Update Car
        private void btUpdateCar_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            Car carCurr = (Car)lvCars.SelectedItem;
            if (carCurr == null) { return; } // should never happen
            try
            {
                carCurr.MakeModel = tbMakeModel.Text;
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        //***Delete Car
        private void btDeleteCar_Click(object sender, RoutedEventArgs e)
        {
            Car carCurr = (Car)lvCars.SelectedItem;
            if (carCurr == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n" + carCurr, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {
                Globals.ctx.Cars.Remove(carCurr);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        //***Selecting item from ListView
        private void lvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvCars.SelectedIndex == -1)
            {
                ClearDialogInputs();
                return;
            }
            Car c = (Car)lvCars.SelectedItem;
            lblId.Content = c.Id;
            tbMakeModel.Text = c.MakeModel;
        }

        //***Exit Dialog
        private void btDone_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
