﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //***Constructor
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new CarsOwnersDbContext(); // ex SystemException
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }

        //***Loading Data from Database
        public void ReloadRecords()
        {
            try
            {
                // if we don't make CarsInGarage virtual we proabably need to load them eagerly using Includ()
                // lvOwners.ItemsSource = Globals.ctx.Owners.Include("CarsInGarage").ToList(); // ex SystemException
                lvOwners.ItemsSource = Globals.ctx.Owners.ToList(); // ex SystemException
                Utils.AutoResizeColumns(lvOwners);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //***Validate All Inputs method
        bool AreInputsValid()
        {
            List<string> errorsList = new List<string>();
            if (tbOwnerName.Text.Length < 2 || tbOwnerName.Text.Length > 100)
            {
                errorsList.Add("Name must be between 2 and 100 characters");
            }
            if (currOwnerImage == null)
            {
                errorsList.Add("You must choose a picture");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        //***Add photo
        byte[] currOwnerImage; // currently selected image, null if none
        private void btImageButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            // dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currOwnerImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currOwnerImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        //***Clear All Inputs method
        public void ClearInputs()
        {
            tbOwnerName.Text = "";
            imageViewer.Source = null;
            tbImage.Visibility = Visibility.Visible;
        }

        //***Add Owner
        private void btAddOwner_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) return;
            try
            {
                Owner owner = new Owner
                {
                    Name = tbOwnerName.Text,
                    Photo = currOwnerImage,
                }; // ex ArgumentNullException , ArgumentException, OverflowException
                Globals.ctx.Owners.Add(owner);
                Globals.ctx.SaveChanges();
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //***Update Owner
        private void btUpdateOwner_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            Owner ownerCurr = (Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; } // should never happen

            try
            {
                ownerCurr.Name = tbOwnerName.Text;
                ownerCurr.Photo = currOwnerImage;
                Globals.ctx.SaveChanges();
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //***Delete Owner
        private void btDeleteOwner_Click(object sender, RoutedEventArgs e)
        {
            Owner ownerCurr = (Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; } // should never happen

            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n" + ownerCurr, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // action cancelled
            try
            {
                Globals.ctx.Owners.Remove(ownerCurr);
                Globals.ctx.SaveChanges();
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //***Manage Cars (Add, update, delete cars)
        private void btManageCars_Click(object sender, RoutedEventArgs e)
        {
            Owner owner = (Owner)lvOwners.SelectedItem;
            if (owner == null) return;
            CarsDialog carDialog = new CarsDialog(owner) { Owner = this };
            carDialog.ShowDialog();
            ReloadRecords();
        }

        //***Selecting item on ListView
        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvOwners.SelectedIndex == -1)
            {
                ClearInputs();
                return;
            }
            try
            {
                Owner owner = (Owner)lvOwners.SelectedItem;
                lblOwnerId.Content = owner.Id;
                tbOwnerName.Text = owner.Name;
                currOwnerImage = owner.Photo;
                imageViewer.Source = Utils.ByteArrayToBitmapImage(owner.Photo); // ex
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //***Export Selected items
        private void btExportSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvOwners.SelectedItems;
            if (selItems.Count == 0)
            {
                return; //should nrver happen!
            }
            SaveFileDialog exportDialog = new SaveFileDialog();
            exportDialog.Filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            exportDialog.Title = "Export to file";
            exportDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            if (exportDialog.ShowDialog() == true)
            {
                try
                {
                    // FIXME: force quoting
                    var config = new CsvConfiguration(CultureInfo.InvariantCulture) { Delimiter = ";", Encoding = Encoding.UTF8, HasHeaderRecord = true };
                    // FIXME: find out how to force date format in the new csvhelper
                    // var options = new TypeConverterOptions { Formats = new[] { "yyyy-MM-dd" } };
                    //csv.Configuration.TypeConverterOptionsCache.AddOptions<DateTime>(options);
                    using (StreamWriter writer = new StreamWriter(exportDialog.FileName))
                    using (CsvWriter csv = new CsvWriter(writer, config))
                    {
                        csv.Context.RegisterClassMap<OwnerExportMap>();
                        csv.WriteRecords(selItems);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error exporting to csv: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        public sealed class OwnerExportMap : ClassMap<Owner>
        {
            public OwnerExportMap()
            {
                Map(m => m.Id);
                Map(m => m.Name);
                Map(m => m.CarsNumber);
            }
        }
    }
}
