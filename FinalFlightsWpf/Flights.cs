﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FinalFlightsWpf
{
    public class Flights
    {
        public enum EType { Domestic, International, Private };

        //Constructors
        public Flights(DateTime onDay, string fromCode, string toCode, EType type, int passengers)
        {
            this.OnDay = onDay;
            this.FromCode = fromCode;
            this.ToCode = toCode;
            this.Type = type;
            this.Passengers = passengers;
        }

        public Flights(string dataLine)
        {
            try
            {
                string[] splitLine = dataLine.Split(';');
                if (splitLine.Length != 5)
                {
                    throw new ArgumentException("Invalid number of items in line");
                }
                OnDay = DateTime.ParseExact(splitLine[0], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                FromCode = splitLine[1];
                ToCode = splitLine[2];
                Type = (EType)Enum.Parse(typeof(EType), splitLine[3]);
                Passengers = int.Parse(splitLine[4]);
            }
            catch (Exception ex) when (ex is FormatException || ex is ArgumentNullException)
            {
                throw new FormatException("Data format invalid in line", ex);
                throw new ArgumentNullException("Error", ex);
            }
        }

        //Fields-Properties
        private DateTime _onDay;
        public DateTime OnDay
        {
            get
            {
                return _onDay;
            }
            set
            {
                _onDay = value;
            }
        }
        private string _fromCode;
        public string FromCode
        {
            get
            {
                return _fromCode;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3,5}$"))
                {
                    throw new ArgumentException("City code must be 3-5 uppercase letters");
                }
                _fromCode = value;
            }
        }
        private string _toCode;
        public string ToCode
        {
            get
            {
                return _toCode;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3,5}$"))
                {
                    throw new ArgumentException("City code must be 3-5 uppercase letters");
                }
                _toCode = value;
            }
        }
        private EType _type;
        public EType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }
        private int _passengers;
        public int Passengers
        {
            get
            {
                return _passengers;
            }
            set
            {
                _passengers = value;
            }
        }

        //ToDataString
        public string ToDataString()
        {
            return $"{OnDay:yyyy-MM-dd};{FromCode};{ToCode};{Type};{Passengers}";
        }
    }
}
