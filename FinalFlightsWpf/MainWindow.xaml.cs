﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalFlightsWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //***List and path(for saving)
        List<Flights> FlightList = new List<Flights>();
        const string path = @"..\..\flights.txt";
        public MainWindow()
        {
            InitializeComponent();
            lvFlights.ItemsSource = FlightList;
            ReadFlightsFromFile();
            lblTotalFlights.Text = "Total Flights: " + FlightList.Count;
        }

        //***Read flights from file
        void ReadFlightsFromFile()
        {
            try
            {
                if (!File.Exists(path))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(path); // IOException and SystemException
                foreach (String line in readText)
                {
                    try
                    {
                        Flights flight = new Flights(line);
                        FlightList.Add(flight);
                        lvFlights.Items.Refresh();
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        //***Write todos to file
        void SaveFlightsToFile(string fileName, List<Flights> flightsToSave)
        {
            try
            {
                List<string> createText = new List<string>();
                foreach (Flights flight in flightsToSave)
                {
                    createText.Add(flight.ToDataString());
                }
                File.WriteAllLines(fileName, createText); // IOException and SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

        }

        //***Exporting multiple lines
        private void SaveSelectedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Process save file dialog box results
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    var selItems = lvFlights.SelectedItems.Cast<Flights>().ToList();
                    if (selItems.Count == 0)
                    {
                        MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    // Save document
                    string fileName = dlg.FileName;
                    SaveFlightsToFile(fileName, selItems);
                }
                catch (Exception ex) when (ex is IOException || ex is SystemException)
                {
                    MessageBox.Show("Error " + ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

            }
        }

        //***Save upon Closing window
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to save before closing?",
            "Confirmation", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Yes)
            {
                SaveFlightsToFile(path, FlightList);
            }
            else if (result == MessageBoxResult.No)
            {
                // No code here  
            }
            else
            {
                e.Cancel = true;
            }
        }

        //Exit
        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //***Add Flight
        private void AddFlightMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                FlightList.Add(dlg.currFlight);
                lblTotalFlights.Text = "Total Flights: " + FlightList.Count;
                lvFlights.Items.Refresh();
            }
        }

        //Update Flight
        private void lvFlights_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvFlights.SelectedItems.Count == 0)
            {
                return;
            }
            Flights flight = (Flights)lvFlights.SelectedItem;
            AddEditDialog dlg = new AddEditDialog(flight);
            dlg.dpOnDay.SelectedDate = FlightList[lvFlights.SelectedIndex].OnDay;
            dlg.tbFrom.Text = FlightList[lvFlights.SelectedIndex].FromCode;
            dlg.tbTo.Text = FlightList[lvFlights.SelectedIndex].ToCode;
            dlg.cbType.Text = FlightList[lvFlights.SelectedIndex].Type.ToString();
            dlg.sliderPassengers.Value = FlightList[lvFlights.SelectedIndex].Passengers;
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                lblTotalFlights.Text = "Total Flights: " + FlightList.Count;
                lvFlights.Items.Refresh();
            }
        }

        //***Delete Flight
        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if(lvFlights.SelectedItems.Count > 1)
            {
                MessageBox.Show("Can only delete one item at a time", "Input Error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            MessageBoxResult result = MessageBox.Show("Do you want to delete this flight?",
            "Confirmation", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                FlightList.Remove(FlightList[lvFlights.SelectedIndex]);
                lvFlights.Items.Refresh();
            }
            else
            {
                return;
            }
        }

        
    }
}
