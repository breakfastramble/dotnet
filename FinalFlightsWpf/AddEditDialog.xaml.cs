﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FinalFlightsWpf
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        public Flights currFlight; // for editing/updating a flight
        public AddEditDialog(Flights flight = null)
        {
            InitializeComponent();
            cbType.ItemsSource = Enum.GetValues(typeof(Flights.EType)).Cast<Flights.EType>();
            if (flight != null)
            {
                currFlight = flight;
                btSave.Content = "Update Flight";
            }
            else
            {
                btSave.Content = "Add Flight";
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime onDay = (DateTime)dpOnDay.SelectedDate;
                string fromCode = tbFrom.Text;
                string toCode = tbTo.Text;
                Flights.EType type = (Flights.EType)Enum.Parse(typeof(Flights.EType), cbType.Text, true);
                int passengers = (int)sliderPassengers.Value;
                if (currFlight == null)
                {
                    currFlight = new Flights(onDay, fromCode, toCode, type, passengers); // ex ArgumentNullException , ArgumentException, OverflowException
                }
                else
                { // ex ArgumentNullException , ArgumentException, OverflowException
                    currFlight.OnDay = onDay;
                    currFlight.FromCode = fromCode;
                    currFlight.ToCode = toCode;
                    currFlight.Type = type;
                    currFlight.Passengers = passengers;
                }
                DialogResult = true;
            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException || ex is OverflowException)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }
    }
}
