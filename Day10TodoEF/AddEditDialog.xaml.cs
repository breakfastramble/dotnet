﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day10TodoEF
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        //***Create object to update
        public Todo currTodo; // null if adding, non-null if editing

        //***Constructor
        public AddEditDialog(Todo todo = null)
        {
            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.EStatus)).Cast<Todo.EStatus>();
            if (todo != null)
            {
                currTodo = todo; // save todo being edited
                tbTask.Text = todo.Task;
                dpDueDate.SelectedDate = todo.DueDate;
                comboStatus.SelectedValue = todo.Status;
                btAction.Content = "Update Todo";
            }
            else
            {
                btAction.Content = "Add Todo";
            }
        }

        //***Save action button (Add or Update)
        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tbTask.Text;
                DateTime dueDate = (DateTime)dpDueDate.SelectedDate;
                Todo.EStatus status = (Todo.EStatus)comboStatus.SelectedValue; //ex
                if (currTodo == null)
                {
                    currTodo = new Todo {Task = task, DueDate = dueDate, Status = status }; // ex ArgumentNullException , ArgumentException, OverflowException
                }
                else
                { // ex ArgumentNullException , ArgumentException, OverflowException
                    currTodo.Task = task;
                    currTodo.DueDate = dueDate;
                    currTodo.Status = status;
                    
                }
                DialogResult = true; // only dismiss the dialog if there were no exceptions
            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException || ex is OverflowException)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }
    }
}
