﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10TodoEF
{
    public class Todo
    {
        //Fields/Properties
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Task { get; set; }

        [Required]
        public DateTime DueDate { get; set; }

        [NotMapped] // make sure this property never is reflected in the database
        public string DueDateCurrCulture => $"{DueDate:d}";

        [Required]
        [EnumDataType(typeof(EStatus))]
        public EStatus Status{ get; set; }
        public enum EStatus { Pending = 1, Done = 2, Delegated = 3};

        //ToString
        public override string ToString()
        {
            return $"{Id}: {Task} due: {DueDate:d}; {Status}";
        }
    }
}
