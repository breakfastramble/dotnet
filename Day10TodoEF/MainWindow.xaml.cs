﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10TodoEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //***Create context object
        public TodosDbContext ctx;

        //***Constructor
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                ctx = new TodosDbContext();
                lvTodos.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation failed: \n" + ex.Message);
            }
        }

        //***Exit program through menu
        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //***Add a Todo
        private void miAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true) // confirmed
            {
                try
                {
                    ctx.Todos.Add(dlg.currTodo); // add todo that was created in the dialog
                    ctx.SaveChanges();
                    lvTodos.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
                    lblStatus.Content = "New Todo added";
                }
                catch (SystemException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    MessageBox.Show("Database operation failed: \n" + ex.Message);
                }
            }
        }

        //***Update a Todo
        private void lvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvTodos.SelectedItems.Count == 0) return;
            Todo todo = (Todo)lvTodos.SelectedItem;
            AddEditDialog dlg = new AddEditDialog(todo);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true) // confirmed
            {
                ctx.SaveChanges();
                lblStatus.Content = "Todo updated";
                lvTodos.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
            }
        }

        //***Delete a Todo
        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Todo todo = (Todo)lvTodos.SelectedItem;
            AddEditDialog dlg = new AddEditDialog(todo);
            if (dlg.currTodo == null) return;
            var result = MessageBox.Show(this, "Are you sure you want to delete this record?\n" + dlg.currTodo, "Confirmation required", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                ctx.Todos.Remove(dlg.currTodo); // schedule attached entity for deletion
                ctx.SaveChanges();
                lvTodos.ItemsSource = (from p in ctx.Todos select p).ToList<Todo>();
            }
        }

        //***Closing window prompt
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "Do you want to leave?";
            MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
