﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Program
    {
        //List
        static List<Person> PeopleList = new List<Person>();

        //Path to people.txt
        static string path = @"..\..\people.txt";
        

        //Read from file
        static void ReadDataFromFile()
        {
            try
            {
                if (!File.Exists(path))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(path); // IOException and SystemException
                foreach (String line in readText)
                {
                    string type = line.Split(';')[0];
                    try
                    {
                        switch(type)
                        {
                            case "Person":
                                Person person = new Person(line);
                                PeopleList.Add(person);
                                break;
                            case "Teacher":
                                Teacher teacher = new Teacher(line);
                                PeopleList.Add(teacher);
                                break;
                            case "Student":
                                Student student = new Student(line);
                                PeopleList.Add(student);
                                break;
                            default:
                                break;
                        }
                        
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        Console.WriteLine($"Error: {ex.Message} in:\n {line}");
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }

        }

        //Calculate Student GPA
        static double gpaSum;
        static int studentCount;
        static void StudentGPACalc()
        {
            foreach(Person student in PeopleList)
            {
                if(student.GetType() == typeof(Student))
                {
                    Student s = (Student)student;
                    gpaSum += s.GPA;
                    studentCount++;
                }
            }
            double averageGPA = gpaSum / studentCount;
            Console.WriteLine($"The average GPA for the students is {averageGPA}");
        }

        //Sorting list by name using LINQ and storing data in a file
        static string path2 = @"..\..\byname.txt";
        static void SortByNameIntoFile()
        {
            try
            {
                var linesList = PeopleList.OrderBy(p => p.Name).Select(p => p.ToDataString());
                File.WriteAllLines(path2, linesList); //IOException
                Console.WriteLine("Data saved to file, sorted by name.");
            } catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing data to file (sorted by name): " + ex.Message);
            }
            
        }

        static void Main(string[] args)
        {
            Logging.ChooseDelegateSetup();
            ReadDataFromFile();
            Console.WriteLine("Here is a list of everyone:");
            foreach(Person person in PeopleList)
            {
                Console.WriteLine(person.ToString());
                
            }
            Console.WriteLine();
            Console.WriteLine("Here is a list of every teacher:");
            foreach (Person teacher in PeopleList)
            {
                if (teacher is Teacher)
                {
                    Console.WriteLine(teacher.ToString());
                }
            }
            Console.WriteLine();
            Console.WriteLine("Here is a list of every student:");
            foreach (Person student in PeopleList)
            {
                if (student is Student)
                {
                    Console.WriteLine(student.ToString());
                };
            }
            Console.WriteLine();
            Console.WriteLine("Here is a list of every person:");
            foreach (Person person in PeopleList)
            {
                if (person.GetType() == typeof(Person))
                {
                    Console.WriteLine(person.ToString());
                };
            }
            Console.WriteLine();
            StudentGPACalc();
            Console.WriteLine();
            SortByNameIntoFile();
            Console.WriteLine();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
