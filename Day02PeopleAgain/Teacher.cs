﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    public class Teacher : Person
    {
        //Constructors
        public Teacher(string subject, int yoe, string name, int age) : base(name, age)
        {
            Subject = subject;
            YearsOfExperience = yoe;
        }
        public Teacher(string dataLine) : base()
        {
            string[] splitLine = dataLine.Split(';');
            if (splitLine.Length != 5)
            {
                Logging.LogFailure?.Invoke("Teacher data line must have 5 fields");
                throw new ArgumentException("Invalid number of items in line");
            }
            Name = splitLine[1];
            if (!int.TryParse(splitLine[2], out int age)) //FormatException
            {
                Logging.LogFailure?.Invoke("Age in data line must be an integer");
                throw new ArgumentException("Age in split line data must be an integer");
            }
            Age = age;
            Subject = splitLine[3];
            if (!int.TryParse(splitLine[4], out int yoe)) //FormatException
            {
                Logging.LogFailure?.Invoke("Years of Experience in data line must be an integer");
                throw new ArgumentException("Years of Experience in split line data must be an integer");
            }
            YearsOfExperience = yoe;
        }

        //Fields/Properties
        private string _subject;
        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                if(value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    Logging.LogFailure?.Invoke("Subject must be 1-50 characters, no semicolons");
                    throw new ArgumentException("Subject must be between 1 and 50 characters and must not contain a semicolon");
                }
                _subject = value;
            }
        }
        private int _yearsOfExperience;
        int YearsOfExperience
        {
            get
            {
                return _yearsOfExperience;
            }
            set
            {
                if(value < 0 || value > 100)
                {
                    Logging.LogFailure?.Invoke("Years of experience must be 0-100");
                    throw new ArgumentException("Years of experience must be between 0 and 100");
                }
                _yearsOfExperience = value;
            }
        }

        //toString
        public override string ToString()
        {
            return $"Teacher {Name} is {Age} and has taught {Subject} for {YearsOfExperience} years";
        }

        //toDataString
        public override string ToDataString()
        {
            return $"Teacher;{Name};{Age};{Subject};{YearsOfExperience}";
        }
    }
}
