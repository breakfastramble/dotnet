﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{

    class Student : Person
    {
        //Constructors
        public Student(string program, int gpa, string name, int age) : base(name, age)
        {
            Program = program;
            GPA = gpa;
        }
        public Student(string dataLine) : base()
        {
            string[] splitLine = dataLine.Split(';');
            if (splitLine.Length != 5)
            {
                Logging.LogFailure?.Invoke("Student data line must have 5 fields");
                throw new ArgumentException("Invalid number of items in line");
            }
            Name = splitLine[1];
            if (!int.TryParse(splitLine[2], out int age)) //FormatException
            {
                Logging.LogFailure?.Invoke("Age in data line must be an integer");
                throw new ArgumentException("Age in split line data must be an integer");
            }
            Age = age;
            Program = splitLine[3];
            if (!double.TryParse(splitLine[4], out double gpa)) //FormatException
            {
                Logging.LogFailure?.Invoke("GPA in data line must be an integer");
                throw new ArgumentException("GPA in split line data must be an integer");
            }
            GPA = gpa;
        }

        //Fields/Properties
        private string _program;
        string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    Logging.LogFailure?.Invoke("Program must be 1-50 characters, no semicolons");
                    throw new ArgumentException("Program must be between 1 and 50 characters and must not contain a semicolon");
                }
                _program = value;
            }
        } 

        private double _gpa;
        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value < 0 || value > 4.3)
                {
                    Logging.LogFailure?.Invoke("GPA must be between 0 and 4.3");
                    throw new ArgumentException("GPA must be between 0 and 4.3");
                }
                _gpa = value;
            }
        }

        //toString
        public override string ToString()
        {
            return $"Student {Name} is {Age} and is studying {Program} with a GPA of {GPA}";
        }

        //toDataString
        public override string ToDataString()
        {
            return $"Student;{Name};{Age};{Program};{GPA}";
        }
    }
}
