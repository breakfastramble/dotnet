﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    public class Person
    {
        //Constructors
        protected Person() { }
        public Person(string name, int age) // throws ArgumentException
        {
            Name = name;
            Age = age;
        }
        public Person(string dataLine)
        {
            string[] splitLine = dataLine.Split(';');
            if (splitLine.Length != 3)
            {
                Logging.LogFailure?.Invoke("Person data line must have 3 fields");
                throw new ArgumentException("Invalid number of items in line");
            }
            Name = splitLine[1];
            if(!int.TryParse(splitLine[2], out int age)) //FormatException
            {
                Logging.LogFailure?.Invoke("Age in data line must be an integer");
                throw new ArgumentException("Age in split line data must be an integer");
            }
            Age = age; 
        }

        //Fields/Properties
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if(value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    Logging.LogFailure?.Invoke("Name must be 1-50 characters, no semicolons");
                    throw new ArgumentException("Name must be between 1 and 50 characters, and not contain a semicolon");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    Logging.LogFailure?.Invoke("Age must be 0-150");
                    throw new ArgumentException("Age must be between 0 and 150 years");
                }
                _age = value;
            }
        }

        //toString
        public override string ToString()
        {
            return $"Person {Name} is {Age} y/o";
        }

        //toDataString
        public virtual string ToDataString()
        {
            return $"Person;{Name};{Age}";
        }
    }
}
