﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06AllInputsWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Register> RegisterList = new List<Register>();
        static string path = @"..\..\register.txt";
        public MainWindow()
        {
            InitializeComponent();
        }

        public void WriteToFile()
        {
            try
            {
                List<string> createText = new List<string>();
                foreach (Register person in RegisterList)
                {
                    string personStr = person.toDataString();
                    createText.Add(personStr);
                }
                File.WriteAllLines(path, createText); // IOException and SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing to file: " + ex.Message);
            }
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            //Name
            if(tbName.Text.Contains(";") || tbName.Text == "")
            {
                MessageBox.Show("Please enter a name, with no semicolons", "Input Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            string name = tbName.Text;

            //Age
            string age = null;
            if((bool)rbUnder18.IsChecked)
            {
                age = "Under 18";
            } else if((bool)rb18To35.IsChecked)
            {
                age = "18 to 35";
            } else if((bool)rbAbove36.IsChecked)
            {
                age = "36 or Above";
            } else
            {
                MessageBox.Show("Error reading radio button", "Input Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            //Pets
            List<string> PetsList = new List<string>();
            
            if((bool)cbCat.IsChecked)
            {
                PetsList.Add("Cat");
            }
            if ((bool)cbDog.IsChecked)
            {
                PetsList.Add("Dog");
            }
            if ((bool)cbOther.IsChecked)
            {
                PetsList.Add("Other");
            }
            string pets = string.Join<string>(", ", PetsList);

            //Continent
            string continent = comboContinent.Text;
            if(continent == "")
            {
                MessageBox.Show("Please select a continent of residence", "Input Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            //Preferred Temp
            double temp = sliderTemp.Value;

            //Create object and add to List
            Register person = new Register(name, age, pets, continent, temp);
            RegisterList.Add(person);

            //Clean text fields and indicate person is registered
            tbName.Text = "";
            lblRegistered.Content = "Person registered";

            //Write to file
            //File.AppendAllText@"..\..\register.txt",
            WriteToFile();
        }

        private void sliderTemp_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double temp = sliderTemp.Value;
            lblTemp.Content = temp.ToString();
        }
    }
}
