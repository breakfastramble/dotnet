﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06AllInputsWpf
{
    class Register
    {
        //Constructors
        public Register(string name, string age, string pets, string continent, double temp)
        {
            Name = name;
            Age = age;
            Pets = pets;
            Continent = continent;
            Temp = temp;
        }
        public Register(string dataLine)
        {
            string[] splitLine = dataLine.Split(';');
            if (splitLine.Length != 5)
            {
                throw new ArgumentException("Invalid number of items in line");
            }
            Name = splitLine[0];
            Age = splitLine[1];
            Pets = splitLine[2];
            Continent = splitLine[3];
            string tempStr = splitLine[4];
            if(!double.TryParse(tempStr, out double temp))
            {
                Console.WriteLine("Error parsing temperature");
            }
            Temp = temp;
        }

        //Fields/Properties
        string Name;
        string Age;
        string Pets;
        string Continent;
        double Temp;

        //ToString and ToDataString
        public override string ToString()
        {
            return $"{Name} is {Age}. Pets: {Pets}. Lives in {Continent}. Preferred temperature: {Temp}";
        }

        public string toDataString()
        {
            return $"{Name};{Age};{Pets};{Continent};{Temp}";
        }
    }
}
