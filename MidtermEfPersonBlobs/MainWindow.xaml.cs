﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MidtermEfPersonBlobs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //***Constructor
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new PersonImageDbContext();
                lvPerson.ItemsSource = Globals.ctx.People.ToList(); // ex SystemException
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }

        }

        private void miAddPerson_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddPersonDialog dlg = new AddPersonDialog();
                dlg.Owner = this;
                if (dlg.ShowDialog() == true)
                {
                    Globals.ctx.People.Add(dlg.currPerson);
                    Globals.ctx.SaveChanges();
                    lvPerson.ItemsSource = Globals.ctx.People.ToList(); // ex SystemException
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lvPerson_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Person person = (Person)lvPerson.SelectedItem;
                if (person == null) return;
                AddPassportPhotoDialog dlg = new AddPassportPhotoDialog(person.passportImage) { Owner = this };
                dlg.Owner = this;
                if (dlg.ShowDialog() == true)
                {
                    Globals.ctx.PassportImages.Add(dlg.currPassImg);
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
