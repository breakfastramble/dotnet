﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MidtermEfPersonBlobs
{
    /// <summary>
    /// Interaction logic for AddPassportPhotoDialog.xaml
    /// </summary>
    public partial class AddPassportPhotoDialog : Window
    {
        public PassportImage currPassImg;
        public AddPassportPhotoDialog(PassportImage passImg)
        {
            InitializeComponent();
            if (passImg != null)
            {
                try
                {
                    currPassImg = passImg;
                    //lblName.Content = currPassImg.person.Name; //Can't seem to get this part to work
                    tbPassport.Text = currPassImg.PassportNo;
                    currPersonImage = currPassImg.Photo;
                    imageViewer.Source = Utils.ByteArrayToBitmapImage(currPassImg.Photo); // ex
                    btSave.Content = "Update";
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                btSave.Content = "Add";
            }
        }

        //***Validate All Inputs method
        bool AreInputsValid()
        {
            List<string> errorsList = new List<string>();
            /*if (!Regex.IsMatch(tbPassport.ToString(), @"^[A-Z]{2}[0-9]{6}$"))
            {
                errorsList.Add("Passport number must be two capital letters followed by 6 numbers");
            }*/ //can't get regex to work properly (no time to fix it!)
            try
            {
                if (btImage.Content == null)
                {
                    errorsList.Add("Must add photo");
                }
            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is FormatException || ex is OverflowException)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        //***Add photo
        byte[] currPersonImage; // currently selected image, null if none
        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currPersonImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currPersonImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!AreInputsValid()) { return; }
                string passport = tbPassport.Text;
                currPassImg.Photo = currPersonImage;
                if (currPassImg == null)
                {
                    currPassImg = new PassportImage { PassportNo = passport, Photo = currPersonImage }; // ex ArgumentNullException , ArgumentException, OverflowException
                }
                else
                { // ex ArgumentNullException , ArgumentException, OverflowException
                    currPassImg.PassportNo = passport;
                    currPassImg.Photo = currPersonImage;
                }
                DialogResult = true;
            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException || ex is OverflowException)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }
    }
}
