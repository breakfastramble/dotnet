﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MidtermEfPersonBlobs
{
    /// <summary>
    /// Interaction logic for AddPersonDialog.xaml
    /// </summary>
    public partial class AddPersonDialog : Window
    {
        public Person currPerson;
        public AddPersonDialog()
        {
            InitializeComponent();
        }

        //***Validate All Inputs method
        bool AreInputsValid()
        {
                List<string> errorsList = new List<string>();
                if (tbName.Text.Length < 2 || tbName.Text.Length > 100)
                {
                    errorsList.Add("Name must be between 2 and 100 characters");
                }
                try
                {
                    int age = Int32.Parse(tbAge.ToString()); //ex ArgumentNullException, FormatException, OverflowException
                if (age < 0 || age > 120)
                    {
                        errorsList.Add("Age must be between 0 and 120");
                    }
                }
                catch (Exception ex) when (ex is ArgumentNullException || ex is FormatException || ex is OverflowException)
                {
                    MessageBox.Show(ex.Message, "Error Message");
                }
                if (errorsList.Count > 0)
                {
                    MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                return true;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
                string name = tbName.Text;
                if(!int.TryParse(tbAge.Text, out int age))
                {
                    MessageBox.Show("Error parsing age", "Error Message");
                }
                currPerson = new Person { Name = name, Age = age };
                DialogResult = true;
        }
    }
}
