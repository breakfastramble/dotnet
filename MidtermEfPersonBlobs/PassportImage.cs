﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermEfPersonBlobs
{
    public class PassportImage
    {
        public int Id { get; set; }
        public string PassportNo { get; set; }
        public int PersonId { get; set; }

        [Required]
        public byte[] Photo { get; set; }

        [Required]
        public Person person { get; set; }
    }
}
