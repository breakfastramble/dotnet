using System;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace MidtermEfPersonBlobs
{
    public class PersonImageDbContext : DbContext
    {
        // Your context has been configured to use a 'Model1' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MidtermEfPersonBlobs.Model1' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Model1' 
        // connection string in the application configuration file.
        const string DbName = "MidtermEfPersonPassport.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public PersonImageDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<PassportImage> PassportImages { get; set; }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}