﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    class Person
    {
        //Fields/Properties and Validation

        //public string Name; // Name 2-100 characters long, not containing semicolons
        //public int Age; // Age 0-150
        //public string City; // City 2-100 characters long, not containing semicolons
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("Name must be between 2 and 100 characters long, and must not contain a semicolon");
                }
                    _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentException("Age must be between 0 and 150 years");
                }
                    _age = value;
            }
        }
        private string _city;
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("City must be between 2 and 100 charaters long, and must not contain a semicolon");
                }
                    _city = value;
            }
        }

        //Constructors
        public Person(string name, int age, string city) //throws Argument Exception
        {
            Name = name;
            Age = age;
            City = city;
        }

        public Person(string dataLine)
        {
            string[] splitLine = dataLine.Split(';');
            if (splitLine.Length != 3)
            {
                throw new ArgumentException("Invalid number of items in line");
            }
            Name = splitLine[0];
            string ageStr = splitLine[1];
            Age = int.Parse(ageStr); //FormatException
            City = splitLine[2];
        }

        //toString
        public override string ToString()
        {
            return $"{Name} is {Age} from {City}";
        }

        //toDataString
        public string toDataString()
        {
            return $"{Name};{Age};{City}";
        }
    }
}
