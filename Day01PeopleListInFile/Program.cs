﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    class Program
    {
        static void Menu()
        {
            int choice = 0;
            do
            {
                Console.WriteLine("Please make a choice [0-4]");
                Console.WriteLine("1. Add person info");
                Console.WriteLine("2. List person's info");
                Console.WriteLine("3. Find a person by name");
                Console.WriteLine("4. Find all persons younger than age");
                Console.WriteLine("0. Exit");
                Console.Write("Please enter your choice: ");
                string choiceStr = Console.ReadLine();
                if (!int.TryParse(choiceStr, out choice))
                {
                    Console.WriteLine("Invalid input, enter an integer between 0 and 4");
                }

                //switch case
                if (choice < 0 || choice > 4)
                {
                    Console.WriteLine("Please enter a value between 0 and 4");
                }
                switch(choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        break;
                    default:
                        Console.WriteLine("Invalid choice, try again");
                        break;
                }
            } while (choice != 0);
            
            


        }
        static void AddPersonInfo()
        {
            try
            {
                Console.Write("Enter person's name: ");
                string name = Console.ReadLine();
                Console.Write("Enter person's age: ");
                string ageStr = Console.ReadLine();
                int age = int.Parse(ageStr);
                Console.Write("Enter city where person lives: ");
                string city = Console.ReadLine();
                Person person = new Person(name, age, city); // create person object
                People.Add(person); // add person to list
            } catch (ArgumentException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            } catch(FormatException ex)
            {
                Console.WriteLine("Error: invalid numercial input");
            }
        }
        static void ListAllPersonsInfo()
        {
            Console.WriteLine("Here is a list of every person: ");
            foreach(Person person in People)
            {
                Console.WriteLine(person);
            }
        }
        static void FindPersonByName()
        {
            Console.Write("Enter person's name: ");
            Person result = People.Find(x => x.Name == Console.ReadLine());
            if (People.Contains(result))
            {
                Console.WriteLine("Search result: " + result);
            }
            else
            {
                Console.WriteLine("There is no one by that name");
            }
        }
        static void FindPersonYoungerThan()
        {
            Console.Write("Enter age: ");
            string ageStr = Console.ReadLine();
            int age = int.Parse(ageStr);
            List<Person> result = new List<Person>(People.FindAll(x => x.Age < age));
            foreach(Person person in result)
            {
                Console.WriteLine(person);
            }
        }
        static string path = @"..\..\people.txt";
        static void ReadAllPeopleFromFile()
        {
            try
            {
                if(!File.Exists(path))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(path); // IOException and SystemException
                foreach(String line in readText)
                {
                    try
                    {
                        Person person = new Person(line);
                        People.Add(person);
                    } catch(Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        Console.WriteLine($"Error: {ex.Message} in:\n {line}");
                    }
                }
            } catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }

        }
        static void SaveAllPeopleToFile()
        {
            try
            {
                List<string> createText = new List<string>();
                foreach (Person person in People)
                {
                    string personStr = person.toDataString();
                    createText.Add(personStr);
                }
                File.WriteAllLines(path, createText); // IOException and SystemException
            } catch(Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing to file: " + ex.Message);
            }
            
        }

        static List<Person> People = new List<Person>();
        static void Main(string[] args)
        {
            ReadAllPeopleFromFile();
            Menu();
            SaveAllPeopleToFile();
        }
    }
}
