﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10PeopleEFGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PeopleDbContext ctx;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                ctx = new PeopleDbContext();
                lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed: /n" + ex.Message);
                Environment.Exit(1);
            }

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            if(!int.TryParse(tbAge.Text, out int age))
            {
                MessageBox.Show("Age invalid");
                return;
            }
            try
            {
                Person person = new Person { Name = name, Age = age };
                ctx.People.Add(person);
                ctx.SaveChanges();
                lblId.Content = "-";
                tbName.Text = "";
                tbAge.Text = "";
                //refresh the listview for database
                lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();
            }
            catch(SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation failed: \n" + ex.Message);
            }
            
        }
        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Person person = (Person)lvPeople.SelectedItem;
                Person person2 = (from p in ctx.People where p.Id == person.Id select p).FirstOrDefault<Person>();
                if (person2 != null)
                {
                    string name = tbName.Text;
                    if (!int.TryParse(tbAge.Text, out int age))
                    {
                        MessageBox.Show("Age invalid");
                        return;
                    }
                    person2.Name = name;
                    person2.Age = age;
                    ctx.SaveChanges();
                    lblId.Content = "-";
                    tbName.Text = "";
                    tbAge.Text = "";
                    //refresh the listview for database
                    lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();
                }
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation failed: \n" + ex.Message);
            }


        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Person person = (Person)lvPeople.SelectedItem;
                Person person2 = (from p in ctx.People where p.Id == person.Id select p).FirstOrDefault<Person>();
                if (person2 != null)
                {
                    ctx.People.Remove(person2);
                    ctx.SaveChanges();
                    //refresh the listview for database
                    lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();
                }
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation failed: \n" + ex.Message);
            }
            
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
                Person person = (Person)lvPeople.SelectedItem;
                if (person == null)
                {
                    btAdd.IsEnabled = true;
                    lblId.Content = "-";
                    tbName.Text = "";
                    tbAge.Text = "";
                }
                else
                {
                    btAdd.IsEnabled = false;
                    tbName.Text = person.Name;
                    tbAge.Text = person.Age.ToString();
                }
        }
    }
}
