﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TravelMockUpWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FutureTrips dlg = new FutureTrips();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {

            }
        }

        private void Budget_Click(object sender, RoutedEventArgs e)
        {
            Budget dlg = new Budget();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {

            }
        }
    }
}
