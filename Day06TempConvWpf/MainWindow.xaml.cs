﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06TempConvWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Recalculate()
        {
            // 1. parse the input
            double inVal;
            if(!double.TryParse(tbInput.Text, out inVal))
            {
                tbOutput.Text = "Invalid input";
                return;
            }

            // 2. convert to celsius
            double celc = 0;
            if ((bool)rbInCelc.IsChecked)
            {
                celc = inVal;
            }
            else if ((bool)rbInFah.IsChecked)
            {
                celc = (inVal - 32) * 5 / 9;
            }
            else if ((bool)rbInKel.IsChecked)
            {
                celc = inVal - 273.15;
            } else //Should never happen
            {
                MessageBox.Show("Invalid control flow", "Internal error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            // 3. convert to the output unit
            double outVal = 0;
            string unit = null;
            if ((bool)rbOutCelc.IsChecked)
            {
                outVal = celc;
                unit = "C";
            }
            else if ((bool)rbOutFah.IsChecked)
            {
                outVal = celc * 9 / 5 + 32;
                unit = "F";
            }
            else if ((bool)rbOutKel.IsChecked)
            {
                outVal = celc + 273.15;
                unit = "K";
            }
            else //Should never happen
            {
                MessageBox.Show("Invalid control flow", "Internal error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            // 4. display the result
            string outStr = $"{outVal:0.##} {unit}";
            tbOutput.Text = outStr;
        }

        private void rbInCelc_Checked(object sender, RoutedEventArgs e)
        {
            Recalculate();
        }

        private void rbInFah_Checked(object sender, RoutedEventArgs e)
        {
            Recalculate();
        }

        private void rbInKel_Checked(object sender, RoutedEventArgs e)
        {
            Recalculate();
        }

        private void rbOutCelc_Checked(object sender, RoutedEventArgs e)
        {
            Recalculate();
        }

        private void rbOutFah_Checked(object sender, RoutedEventArgs e)
        {
            Recalculate();
        }

        private void rbOutKel_Checked(object sender, RoutedEventArgs e)
        {
            Recalculate();
        }

        private void tbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            Recalculate();
        }
    }
}
